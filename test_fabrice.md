# Tests Fabrice




## Tests - 30/05/2020

| Imprimante |  |
|--------|---|
| Modèle  | Longer3d LK4PRO - équivalent alphawise u30pro |
| Slicer | Cura 4.5 |
| Buse | 0.4 |
| Plateau | **Souple magnétique** |

### Test 1 [OK] - Standard Quality + Bordure

| Paramètres |    |
|------------|----|
| Profil   | Standart Quality  - 0.2mm |
| Hauteur de couche | 0.2 |
| T° Extrudeur | 195°C |
| T° plateau | 60°C |
| Adhérence | bordure - 8 lignes |
| vitesse d'impression | 65mm/s |
| vitesse d'impression paroi | 50 mm/s |

![](img/tests_fabrice/resultat_test1.jpg)

### Test 2 [FAIL] - Standard Quality - sans adhérence

* la bordure a été desactivée par rapport au test précédent

| Paramètres |    |
|------------|----|
| Profil   | Standart Quality  - 0.2mm |
| Hauteur de couche | 0.2 |
| T° Extrudeur | 197°C |
| T° plateau | 60°C |
| vitesse d'impression | 65mm/s |
| vitesse d'impression paroi | 50 mm/s |
| **PAS D'ADHERENCE**


#### Résultat

* Echec: la cage est tombée.

![](img/tests_fabrice/resultat_test2.jpg)


### Test 3 [NOK] - Standard Quality - sans adhérence - couche initiale renforcée

* la couche initiale est renforcée:
  * hauteur: 0.3
  * température plus éleveée
  * débit plus élevé

| Paramètres |    |
|------------|----|
| Profil   | Standart Quality  - 0.2mm |
| Hauteur de couche | 0.2 |
| Hauteur de initiale | 0.3 |
| Nombre de lignes de la paroi | 5 |
| T° Extrudeur | 197°C |
| T° Extrudeur couche initiale | 200°C |
| T° plateau | 60°C |
| vitesse d'impression | 65mm/s |
| vitesse d'impression paroi | 50 mm/s |
| Décalage en Z lors d'une rétractation | [x] |
| Hauteur du décalage en Z | 0.4 mm |
| Débit de la couche initiale | 105% |

#### Résultat

* la pièce a tenu mais le support magnétique flexible n'était pas, sur les dernières couches, suffisament stable.
* Je vais tester avec un plateau en verre.

![](img/tests_fabrice/resultat_test3_1.jpg)
![](img/tests_fabrice/resultat_test3_2.jpg)

