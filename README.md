# Turgriffe - V5

## Présentation 
Ouvre Porte - Pousse bouton.
Poignée sur le principe de cran d'arrêt pour éviter de contaminer votre poche ou vos mains à l'utilisation.

![](V4_poignee.JPG)

## Montage

* [Vidéo](https://www.youtube.com/watch?v=k6K73AZvEj4&feature=share&fbclid=IwAR197wBz8-6hqvOV99gg0cNSBo9WCdTzgBdzZ3Zg8-SEoQtvZYw32x2ycUc)

## STL

* [cage](STL/V5%20poignée%20cage.STL)
* [couvercle](STL/V5%20couvercle.STL)
* [clé](STL/V4%20poignée%20%20clé.STL)
* [axe](STL/axe.STL)

## Instructions

| Placement impression      |        |
| ----- |------- |
| cage  | couchée sur la paroi lisse |
| cles  | sur le coté lisse |
| couvercle | sur le cote |
| axe   |  sur le coté plat |

![](cura.png)

| Paramètres impression |  buse 0.4  |
|-----------------------|---|
| hauteur de couches   |  0.2 |
| Paroi  | 2 walls |
| 1ere couche  | 100 % |

## Autres versions

* [lister les autres versions](https://framagit.org/makers-67/covid-19/turgriffe/-/branches)